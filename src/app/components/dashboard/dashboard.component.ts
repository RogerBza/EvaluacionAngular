import { Component } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

	public barChartOptions:any = {
    	scaleShowVerticalLines: false,
    	responsive: true
	};

	public barChartLabels:string[] = ['11-02-18', '12-02-18', '13-02-18', '14-02-18', '15-02-18', '16-02-18', '17-02-18'];
	public barChartType:string = 'bar';
	public barChartLegend:boolean = true;
 
	public barChartData:any[] = [
	    {data: [1, 2, 3, 4, 5, 6, 7, 8], label: 'Horas'},
	];

	// events
	  public chartClicked(e:any):void {
	    console.log(e);
	  }
	 
	  public chartHovered(e:any):void {
	    console.log(e);
	  }
	 
	  public randomize():void {
	    // Only Change 3 values
	    let data = [
	      Math.round(Math.random() * 8),
	      6,
	      4	,
	      (Math.random() * 7),
	      6,
	      (Math.random() * 8),
	      4];
	    let clone = JSON.parse(JSON.stringify(this.barChartData));
	    clone[0].data = data;
	    this.barChartData = clone;
	    
	}

}
