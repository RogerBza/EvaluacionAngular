import { RouterModule, Routes } from '@angular/router';

import { InicioComponent } from './components/inicio/inicio.component';
import { AyudaComponent } from './components/ayuda/ayuda.component';
import { PerfilComponent } from './components/perfil/perfil.component';

import { AuthGuardService } from "./service/auth-guard.service";

const APP_ROUTES: Routes = [
	{ path: 'inicio', component: InicioComponent },
	{ path: 'ayuda', component: AyudaComponent },
	{ path: 'perfil', component: PerfilComponent, canActivate: [ AuthGuardService ] },
	{ path: '**', pathMatch: 'full', redirectTo: 'inicio' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);